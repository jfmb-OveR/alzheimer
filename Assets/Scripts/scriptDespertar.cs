﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptDespertar : MonoBehaviour {

    public GameObject goPersonajeDespierto;
    public GameObject goMantas;
    public GameObject goFrase;
    public mySceneManager mySM;
    public GameObject goAudioKnock;

    public scriptArmario myScriptArmario;

    public GameObject goShinyStar;
    public GameObject goSoundFabric;

    private bool bIsAwake = false;
    private bool bCoroutine = true;
    
    private void OnMouseDown()
    {
        goPersonajeDespierto.SetActive(true);
        goMantas.SetActive(false);
        goFrase.SetActive(false);
        goSoundFabric.SetActive(true);
        this.gameObject.SetActive(false); 

        bIsAwake = true;

        myScriptArmario.ActivateCollider();

        goShinyStar.SetActive(false);

        mySM.ActivateObjects(1);
        mySM.ActivateStars(true);
    }

    IEnumerator Wait(float fSeconds)
    {
        yield return new WaitForSeconds(fSeconds);
        bCoroutine = false;
    }

    IEnumerator ShowMessages()
    {
        bCoroutine = true;
        goFrase.SetActive(true);
        goAudioKnock.SetActive(true);
        yield return new WaitForSeconds(5);
        goFrase.SetActive(false);
        goAudioKnock.SetActive(false);
        StartCoroutine(Wait(5));
    }

    void Awake()
    {
        goPersonajeDespierto.SetActive(false);
        goMantas.SetActive(true);
        goSoundFabric.SetActive(false);

    }

    private void Start()
    {
        goAudioKnock.SetActive(false);
        goFrase.SetActive(false);
        goShinyStar.SetActive(true);
        StartCoroutine(Wait(5));
    }

    private void Update()
    {
        if(!bIsAwake && !bCoroutine)
        {
            StartCoroutine(ShowMessages());
        }
    }
}
