﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzlePieceController : MonoBehaviour {

    public PuzzleManager myPuzzleManager;

    public bool bMoving = true;
    public Transform tSlot;
    public float fBaseDistance = 1f;
    public int iPosicionFinal;
    public int iPosicionActual;

    //    public MySceneManager sceneManager;
    private PuzzlePieceController sPiece;


    private float fXPosition;
    private float fYPosition;
    private float fZPosition;
    private float fDistance;
    private int iTempPosition;

    private bool bIamSlot;
    private GameObject goSlot;

    void Awake()
    {
        sPiece = this.GetComponent<PuzzlePieceController>();

        goSlot = tSlot.gameObject;

        if(iPosicionFinal == 9)
            bIamSlot = true;
    }

    public void setNewPosition(int iAntPosition, float newX, float newY)
    {
        iPosicionActual = iAntPosition;

        this.transform.position = new Vector3(newX, newY, 0);
    }

    public bool CheckPiece(){
        return (iPosicionActual == iPosicionFinal);
    }
	
    public void changePieces()
    {
        //                Debug.Log("Moviendo ficha: " + this.transform.position.x + "," + this.transform.position.z);
        fXPosition = this.transform.position.x;
        fYPosition = this.transform.position.y;
        fZPosition = this.transform.position.z;
//        iTempPosition = this.iPosicionActual;
        iTempPosition = this.iPosicionActual;

        //                this.transform.position = new Vector3(tSlot.position.x, tSlot.position.y, tSlot.position.z);
        //                this.iPosicionActual = sPiece.iPosicionActual;

        setNewPosition(goSlot.GetComponent<PuzzlePieceController>().iPosicionActual, tSlot.position.x, tSlot.position.y);

        goSlot.GetComponent<PuzzlePieceController>().setNewPosition(iTempPosition, fXPosition, fYPosition);

//        tSlot.transform.position = new Vector3(fXPosition, fYPosition, fZPosition);

        Debug.Log("Posición final: " + iPosicionFinal + " - Posición actual: " + iPosicionActual);

        myPuzzleManager.checkPieces();
    }

    void OnMouseDown() {
//		if(bMoving == true)
		if(myPuzzleManager.GetAllowPressPieces())
        {
            fDistance = Vector3.Distance(transform.position, tSlot.position);
            if (fDistance <= fBaseDistance)
            {
                changePieces();
            }
        }
    }
}
