﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatePuzzle : ObjectScript {
    public GameObject goMainCamera;
    public GameObject goPuzzle;
    public GameObject goPuzzlePanel;

    //    [Header("Label sobre la foto")]
    //    public GameObject goPhotoDescription;
    //    [Header("Botón de resolución del puzzle")]
    //    public GameObject goButtonSolvePuzzle;

    public void NormalSituation()
    {
        goPuzzle.SetActive(false);
        goPuzzlePanel.SetActive(false);
        goMainCamera.SetActive(true);
    }

    public void OnMouseDown()
    {
        if (myGameManager.myGMInstance.getActivateObjects() == true)
        {
            goMainCamera.SetActive(false);
            goPuzzle.SetActive(true);
            goPuzzlePanel.SetActive(true);
            goShinyStar.SetActive(false);
        }
    }

    void Awake()
    {
        NormalSituation();
        //        goPhotoDescription.SetActive(false);
        //        goButtonSolvePuzzle.SetActive(false);
    }
}
