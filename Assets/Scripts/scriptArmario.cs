﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptArmario : ObjectScript {

    public GameObject goFirstSprite;
    public GameObject goSecondSprite;

    public GameObject goRopaChico;
    public GameObject goRopaChica;
    public GameObject goAudioDresser;

    public void ActivateCollider()
    {
        this.GetComponent<BoxCollider2D>().enabled = true;
    }

    void OnMouseDown()
    {
        goFirstSprite.SetActive(false);
        goSecondSprite.SetActive(true);

        goRopaChico.SetActive(true);
        goRopaChica.SetActive(true);
        goShinyStar.SetActive(false);

        goAudioDresser.SetActive(true);

        this.GetComponent<BoxCollider2D>().enabled = false;
    }

    void Awake()
    {
        myLabel.SetActive(false);

        goFirstSprite.SetActive(true);
        goSecondSprite.SetActive(false);

        goRopaChico.SetActive(false);
        goRopaChica.SetActive(false);

        goAudioDresser.SetActive(false);

        this.GetComponent<BoxCollider2D>().enabled = false;
    }
}
