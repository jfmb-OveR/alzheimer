﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptRopa : ObjectScript {

    public mySceneManager mySM;
    public GameObject goClothesSound;

    void OnMouseDown()
    {
        if(myGameManager.myGMInstance.getActivateObjects() == true){
            goClothesSound.SetActive(true);
            mySM.ShowNextScene();
        }
    }

    void Start(){
        myLabel.SetActive(false);
        goClothesSound.SetActive(false);
    }
}
