﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PuzzleManager : MonoBehaviour {

    [Header("Número de la escena, de 0 a 6")]
    public int iIDEscena = 1;

    [Header("Número de objetos")]
    public int iLength = 8;
    [Header("Controladores de piezas")]
    public PuzzlePieceController[] aPieceController;
    [Header("Sonido Éxito")]
    public AudioSource soundSuccess;
    [Header("Label sobre la foto")]
    public GameObject goPhotoDescription;
    [Header("Botón de resolución del puzzle")]
    public GameObject goSolvePuzzle;

    [Header("Tiempo de espera para botón de resolución")]
    public float fWaitingTime = 60;

    public UnityAds myAd;

    private bool bSceneEnded = false;
    private bool bAllowPressPieces = false;

    private struct structPosition
    {
        float positionX;
        float positionY;

        public void setXY(float newX, float newY)
        {
            positionX = newX;
            positionY = newY;
        }

        public float getX()
        {
            return positionX;
        }

        public float getY()
        {
            return positionY;
        }
    }

    private structPosition[] aPiecesPosition;

    private bool[] aPositionUsed;

    
    public bool GetAllowPressPieces(){
        return bAllowPressPieces;
    }

//    public bool checkPieces()
    public void checkPieces()
    {
        bool bAreOK = true;

        for (int i = 0; (i < iLength) && (bAreOK == true); i++)
        {
            if (aPieceController[i].CheckPiece() == false)
            {
                bAreOK = false;
                //                GameManager.instance.addErrorsInScene(iIDEscena);
            }
            Debug.Log("Pieza " + i + ": " + bAreOK);
        }



        if(bAreOK == true){
            bAllowPressPieces = false;            
            sceneOver();
        }

//        return bAreOK;
    }

    public void resetPositionsUsed()
    {
        for (int i = 0; i < iLength; i++)
            aPositionUsed[i] = false;
    }

    public void resetPieces()
    {
        for (int i = 0; i < aPieceController.Length; i++)
        {
            aPieceController[i].setNewPosition(i, aPiecesPosition[i].getX() + transform.position.x, aPiecesPosition[i].getY() + transform.position.y);
        }
        setEnded(true);
        myAd.ShowAd();
    }

    public void shufflePieces()
    {
        int randomNumber = 0;

        resetPositionsUsed();

        for (int i = 0; i < iLength; i++)
        {
            do
            {
                randomNumber = Random.Range(0, (iLength));
            } while (aPositionUsed[randomNumber] != false);

//            Debug.Log("Pieza: " + i + " posicion: " + randomNumber + " = " + aPiecesPosition[randomNumber].getX() + ", " + aPiecesPosition[randomNumber].getY());

            aPositionUsed[randomNumber] = true;
            aPieceController[i].setNewPosition(randomNumber, aPiecesPosition[randomNumber].getX() + transform.position.x, aPiecesPosition[randomNumber].getY() + transform.position.y);
        }
//        aPositionUsed[8] = true;
        aPieceController[8].setNewPosition(8, aPiecesPosition[8].getX() + transform.position.x, aPiecesPosition[8].getY() + transform.position.y);
        
        bAllowPressPieces = true;
    }

    public void sceneOver()
    {
        Debug.Log("El puzzle está resuelto!!!!!");

        goPhotoDescription.SetActive(true);

        myGameManager.myGMInstance.setPuzzleSolve(iIDEscena, 1);

        if (soundSuccess != null)
            soundSuccess.Play();
    }

    public void setEnded(bool newValue)
    {
        bSceneEnded = newValue;
        sceneOver();
    }

    public void increaseError()
    {
//        GameManager.instance.addErrorsInScene(iIDEscena);
    }

    IEnumerator ShowButtonSolution()
    {
        yield return new WaitForSeconds(fWaitingTime);
        goSolvePuzzle.SetActive(true);
    }

    void OnEnable(){
        StartCoroutine(ShowButtonSolution());
    }

    void Awake()
    {
        goPhotoDescription.SetActive(false);
//        goPuzzlePanel.SetActive(false);
//        goSolvePuzzle.SetActive(false);

        aPositionUsed = new bool[iLength];

        for (int i = 0; i < iLength; i++)
        {
            aPositionUsed[i] = false;
        }

        aPiecesPosition = new structPosition[iLength+1];

        aPiecesPosition[0].setXY(-1, 1);
        aPiecesPosition[1].setXY(0, 1);
        aPiecesPosition[2].setXY(1, 1);
        aPiecesPosition[3].setXY(-1, 0);
        aPiecesPosition[4].setXY(0, 0);
        aPiecesPosition[5].setXY(1, 0);
        aPiecesPosition[6].setXY(-1, -1);
        aPiecesPosition[7].setXY(0, -1);
        aPiecesPosition[8].setXY(1, -1);

        //        resetPieces();

        shufflePieces();
        //        sceneStart();
    }

    void Start()
    {
//         goPuzzlePanel.SetActive(true);
        goSolvePuzzle.SetActive(false);

        StartCoroutine(ShowButtonSolution());
    }
}
