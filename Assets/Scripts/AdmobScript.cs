﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class AdmobScript : MonoBehaviour {    
/*    
    public static AdmobScript myAdmobScript_Instance;
    [Header("Id de la aplicación en Admob")]
    public string appId = "ca-app-pub-9376682217978630~3586999798"; //ID de la aplicación
    [Header("Id del banner en Admob")]
    public string adUnitId = "ca-app-pub-9376682217978630/7207514755";

    private BannerView bannerView;

    private bool bBannerLoaded = false;

    private void RequestBanner()
    {
        if(bBannerLoaded == false)
        { 
            // Create a 320x50 banner at the top of the screen.
            bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
            // Create an empty ad request.
            AdRequest request = new AdRequest.Builder().Build();

            // Load the banner with the request.
            bannerView.LoadAd(request);

            bBannerLoaded = true;
            //        if(bDontDestroyOnLoad == true)
            //            DontDestroyOnLoad(this.gameObject);
        }
    }


    public void HideBanner()
    {
        bannerView.Hide();
    }

    public void ShowBanner()
    {
        bannerView.Show();
    }

    private void OnDestroy()
    {
//            bannerView.Destroy();
    }

    private void Awake()
    {
        if (myAdmobScript_Instance == null)
        {
            myAdmobScript_Instance = this;
            DontDestroyOnLoad(myAdmobScript_Instance);
        }
    }

    void Start()
    {
        if (bBannerLoaded == false)
        {
            // Initialize the Google Mobile Ads SDK.
            MobileAds.Initialize(appId);

            RequestBanner();
            HideBanner();
        }
    }
    */


//ca-app-pub-9376682217978630/4185223990"
    public string sAdID; //ID of the ad block in admob web.

    public static AdmobScript myAdmobScript_Instance;
    private BannerView bannerView;

    public void HideBanner()
    {
        bannerView.Hide();
    }

    public void ShowBanner()
    {
        Debug.Log("--- Showing banner ---");
        bannerView.Show();
        Debug.Log("--- Banner shown ---");
    }

    private void OnDestroy()
    {
//            bannerView.Destroy();
    }

    private void Awake()
    {
        if (myAdmobScript_Instance == null)
        {
            myAdmobScript_Instance = this;
            DontDestroyOnLoad(myAdmobScript_Instance);
        }
    }


    public void Start()
    {
        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(initStatus => { });

        this.RequestBanner();
        this.HideBanner();
    }

    private void RequestBanner()
    {
        #if UNITY_ANDROID
            string adUnitId = sAdID; //ID del banner en Admob
        #else
            string adUnitId = "unexpected_platform";
        #endif

        Debug.Log(adUnitId);

        // Create a 320x50 banner at the top of the screen.
        this.bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        // Load the banner with the request.
        this.bannerView.LoadAd(request);
    }
}
