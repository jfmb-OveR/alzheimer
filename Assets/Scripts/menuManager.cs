﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menuManager : MonoBehaviour {

    [SerializeField]
    private UILabel textLoading;
    //private UILabel labelLoading;
    [SerializeField]
    private GameObject goMainMenu;
    [SerializeField]
    private GameObject goLanguageSelector;
    [SerializeField]
    private GameObject goCharacterPanel;
    [SerializeField]
    private GameObject goCreditsPanel;

    private bool bLoadScene = false;


    public void setEnglish()
    {
        Localization.language = "English";
        ShowHideLanguageSelector();
    }

    public void setSpanish()
    {
        Localization.language = "Spanish";
        ShowHideLanguageSelector();
    }

    public void SetCharacterAnnie()
    {
        myGameManager.myGMInstance.setCharacterWoman(true);
        LoadGame();
    }

    public void SetCharacterFred()
    {
        myGameManager.myGMInstance.setCharacterWoman(false);
        LoadGame();
    }

    public void ShowCharacterPanel()
    {
        goMainMenu.SetActive(false);
        goCharacterPanel.SetActive(true);
    }

    public void ShowHideLanguageSelector()
    {
        goMainMenu.SetActive(!goMainMenu.activeSelf);
        goLanguageSelector.SetActive(!goLanguageSelector.activeSelf);
    }

    public void ShowHideCredits()
    {
        goMainMenu.SetActive(!goMainMenu.activeSelf);
        goCreditsPanel.SetActive(!goCreditsPanel.activeSelf);
    }

    public void LoadGame()
    {
        bLoadScene = true;
        goCharacterPanel.SetActive(false);
        myGameManager.myGMInstance.resetPuzzles();
        StartCoroutine(LoadScene(1));
    }

    public void LoadLastScene()
    {
        StartCoroutine(LoadScene(5));
    }

    public void Quit()
    {
        Application.Quit();
    }

    IEnumerator ShowBanner()
    {
        yield return new WaitForSeconds(5);
        AdmobScript.myAdmobScript_Instance.ShowBanner();
    }

    IEnumerator LoadScene(int newScene)
    {
        AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(newScene);
        while (!async.isDone)
        {
            yield return null;
        }
    }

    void Start()
    {
        //        labelLoading = this.GetComponent<UILabel>();
        textLoading.enabled = false;
        goMainMenu.SetActive(true);
        goLanguageSelector.SetActive(false);
        goCharacterPanel.SetActive(false);
        goCreditsPanel.SetActive(false);

        ShowBanner();
    }

    void Update()
    {
        if (bLoadScene == true)
        {
            textLoading.enabled = true;
            textLoading.color = new Color(textLoading.color.r, textLoading.color.g, textLoading.color.b, Mathf.PingPong(Time.time, 1));
            //            labelLoading.tint = new Color(labelLoading.tint.r, labelLoading.tint.g, labelLoading.tint.b, Mathf.PingPong(Time.time, 1));
        }
    }
}
