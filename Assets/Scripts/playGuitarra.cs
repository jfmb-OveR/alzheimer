﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playGuitarra : ObjectScript {
    public AudioSource[] audioAcordes;
    int iIndex = 0;

    void OnMouseDown()
    {
        if (myGameManager.myGMInstance.getActivateObjects() == true){
            if (iIndex > 3)
                iIndex = 0;

            audioAcordes[iIndex].Play();
            iIndex++;

            goShinyStar.SetActive(false);
        }
    }
}
