﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectScript : MonoBehaviour {

    public GameObject myLabel;
    public GameObject goShinyStar;

    private void OnMouseEnter()
    {
        if(myGameManager.myGMInstance.getActivateObjects() == true)
            myLabel.SetActive(true);
    }

    private void OnMouseDown()
    {
        if(myGameManager.myGMInstance.getActivateObjects() == true)
        {
            myLabel.SetActive(true);
            goShinyStar.SetActive(false);
            StartCoroutine(HideLabel());
        }
    }

    private void OnMouseExit()
    {
        myLabel.SetActive(false);
    }

    IEnumerator HideLabel()
    {
        yield return new WaitForSeconds(5);
        myLabel.SetActive(false);
    }

    void Start()
    {
        myLabel.SetActive(false);
    }

}
