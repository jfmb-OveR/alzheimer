﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class UnityAds : MonoBehaviour, IUnityAdsListener { 
    [SerializeField]
    string gameId;
    [SerializeField]
    string myPlacementId;
    [SerializeField]
    bool testMode = true;

    Button myButton;

    bool bVideoFinished = false;

    // Initialize the Ads listener and service:
    void Start () {
        // Initialize the Ads listener and service:
        Advertisement.AddListener(this);
        Advertisement.Initialize(gameId, testMode);
    }

    public bool VideoFinished(){
        return bVideoFinished;
    }
    public void ShowAd(){   
        bVideoFinished = false;
        StartCoroutine(CorShowVideo());
    }

    IEnumerator CorShowVideo(){
        yield return new WaitForSeconds(1f);
        Advertisement.Show (myPlacementId);
    }

    // Implement IUnityAdsListener interface methods:
    public void OnUnityAdsDidFinish (string myPlacementId, ShowResult showResult) {
        // Define conditional logic for each ad completion status:
        if (showResult == ShowResult.Finished) {
            // Reward the user for watching the ad to completion.
            bVideoFinished = true;
            Debug.Log("Ad finished");
            Debug.Log("Video shown");

        } else if (showResult == ShowResult.Skipped) {
            // Do not reward the user for skipping the ad.
            bVideoFinished = true;
            Debug.Log("Ad skipped");
            Debug.Log("Video shown");
        } else if (showResult == ShowResult.Failed) {
            Debug.LogWarning ("The ad did not finish due to an error. ");
        }
    }

    public void OnUnityAdsReady (string placementId) {
        // If the ready Placement is rewarded, show the ad:
        if (myPlacementId == placementId) {
            Debug.Log("Ads ready");
        }
    }

    public void OnUnityAdsDidError (string message) {
        // Log the error.
        Debug.Log("Error in Ads...");
    }

    public void OnUnityAdsDidStart (string myPlacementId) {
        // Optional actions to take when the end-users triggers an ad.
    } 
}
/*
public class UnityAds : MonoBehaviour
{
    public void ShowAd()
    {
        Debug.Log("He entrado en ShowAd");
        if (!Advertisement.IsReady())
        {
            Debug.Log("Ads not ready for default placement");
            return;
        }
        Advertisement.Show();
        Debug.Log("Mostrando anuncio!!!!!!");
    }
}
*/