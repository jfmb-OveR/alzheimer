﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadLastScene : MonoBehaviour {

    public UILabel textLoading;
    bool bLoadScene = false;

    IEnumerator LoadScene(int newScene)
    {
        bLoadScene = true;
        AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(newScene);
        while (!async.isDone)
        {
            yield return null;
        }
    }

    IEnumerator LoadFinalScene()
    {
        yield return new WaitForSeconds(11);
        StartCoroutine(LoadScene(4));
    }

	// Use this for initialization
	void Start () {
        textLoading.enabled = false;
        StartCoroutine(LoadFinalScene());
	}
	
	// Update is called once per frame
	void Update () {
        if (bLoadScene == true)
        {
            textLoading.enabled = true;
            textLoading.color = new Color(textLoading.color.r, textLoading.color.g, textLoading.color.b, Mathf.PingPong(Time.time, 1));
            //            labelLoading.tint = new Color(labelLoading.tint.r, labelLoading.tint.g, labelLoading.tint.b, Mathf.PingPong(Time.time, 1));
        }
    }
}
