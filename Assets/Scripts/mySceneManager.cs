﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mySceneManager : MonoBehaviour {

    public GameObject goCharacterMan;
    public GameObject goCharacterWoman;
    public GameObject goNextScenePanel;
    public GameObject goEscPanel;
    public int iNextScene;
    public UILabel textLoading;

    public GameObject[] aGoStars;

    protected bool bLoadScene = false;

    public void Begining()
    {
//        myGameManager.myGMInstance.setActivateObjects(false);
    }

    public void LoadNextScene()
    {
        StartCoroutine(LoadScene(iNextScene));
    }

    public void LoadMainMenu()
    {
        StartCoroutine(LoadScene(0));
    }

    public void ShowNextScene()
    {
        goNextScenePanel.SetActive(true);
    }

    public void HideNextScene()
    {
        goNextScenePanel.SetActive(false);
    }

    public void ShowEscPanel()
    {
        goEscPanel.SetActive(true);
    }

    public void HideEscPanel()
    {
        goEscPanel.SetActive(false);
    }

    public void ActivateObjects(float fSeconds)
    {
        StartCoroutine(StartWaitingTime(fSeconds));
    }

    public void ActivateStars(bool bValue){
        Debug.Log("Activar estrellas: " + bValue);
        for (int i = 0; i < aGoStars.Length; i++){
            aGoStars[i].SetActive(bValue);
            Debug.Log("Estrella " + i + ": " + aGoStars[i].activeSelf);
        }
    }

    IEnumerator LoadScene(int newScene)
    {
        bLoadScene = true;
        AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(newScene);
        while (!async.isDone)
        {
            yield return null;
        }
    }


    IEnumerator StartWaitingTime(float fSeconds)
    {
        Begining();

        yield return new WaitForSeconds(fSeconds);
//        Debug.Log("Voy a activar los objetos)");
        myGameManager.myGMInstance.setActivateObjects(true);
    }
    
    // Use this for initialization
    void Start () {
        textLoading.enabled = false;
        myGameManager.myGMInstance.setActivateObjects(false);
        goCharacterWoman.SetActive(myGameManager.myGMInstance.getCharacterWoman());
        goCharacterMan.SetActive(!myGameManager.myGMInstance.getCharacterWoman());

        ActivateStars(false);
        HideNextScene();
        HideEscPanel();

        AdmobScript.myAdmobScript_Instance.HideBanner();

        //StartCoroutine(StartWaitingTime());
    }

    void Update()
    {
        if (bLoadScene == true)
        {
            textLoading.enabled = true;
            textLoading.color = new Color(textLoading.color.r, textLoading.color.g, textLoading.color.b, Mathf.PingPong(Time.time, 1));
            //            labelLoading.tint = new Color(labelLoading.tint.r, labelLoading.tint.g, labelLoading.tint.b, Mathf.PingPong(Time.time, 1));
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ShowEscPanel();
        }
    }
}
