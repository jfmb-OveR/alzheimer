﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playTocadiscos : ObjectScript {

    public GameObject goMusic;
    public GameObject goNotes;

    void ActivateMusicPlayer(bool bValue){
        goMusic.SetActive(bValue);
        goNotes.SetActive(bValue);
    }

    private void OnMouseDown()
    {
        if(myGameManager.myGMInstance.getActivateObjects() == true)
        {
            ActivateMusicPlayer(!goMusic.activeSelf);
            goShinyStar.SetActive(false);
        }
    }

    void Start()
    {
        myLabel.SetActive(false);
        
        ActivateMusicPlayer(true);
    }
}
