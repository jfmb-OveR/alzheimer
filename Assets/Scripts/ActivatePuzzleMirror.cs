﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatePuzzleMirror : ActivatePuzzle {
    public GameObject goLabelScene;

    new public void OnMouseDown()
    {
        if (myGameManager.myGMInstance.getActivateObjects() == true)
        {
            goLabelScene.SetActive(false);
            goMainCamera.SetActive(false);
            goPuzzle.SetActive(true);
            goPuzzlePanel.SetActive(true);
        }
    }
}
