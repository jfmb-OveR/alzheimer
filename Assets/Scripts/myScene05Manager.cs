﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class myScene05Manager : MonoBehaviour {
    public int iNextScene = 6; //Menu without animation
    public UILabel textLoading;
    public GameObject[] aGoSprites;
    public GameObject[] aGoLabels;

    private bool bLoadScene = false;

    public void Salir()
    {
        StartCoroutine(LoadScene(iNextScene));
    }

    IEnumerator LoadScene(int newScene)
    {
        bLoadScene = true;
        AdmobScript.myAdmobScript_Instance.HideBanner();

        AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(newScene);
        while (!async.isDone)
        {
            yield return null;
        }
    }

    void Start()
    {
        textLoading.enabled = false;

        for (int i = 0; i < aGoLabels.Length; i++)
        {
            aGoSprites[i].SetActive(myGameManager.myGMInstance.getPuzzleSolved(i));
            aGoLabels[i].SetActive(myGameManager.myGMInstance.getPuzzleSolved(i));
        }

        AdmobScript.myAdmobScript_Instance.ShowBanner();
    }

    void Update()
    {
        if (bLoadScene == true)
        {
            textLoading.enabled = true;
            textLoading.color = new Color(textLoading.color.r, textLoading.color.g, textLoading.color.b, Mathf.PingPong(Time.time, 1));
            //            labelLoading.tint = new Color(labelLoading.tint.r, labelLoading.tint.g, labelLoading.tint.b, Mathf.PingPong(Time.time, 1));
        }
    }
}
