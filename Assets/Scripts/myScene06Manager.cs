﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class myScene06Manager : mySceneManager {

    public GameObject goNextButton;

    private bool bButtonNextActivated = false;

    void Start()
    {
        textLoading.enabled = false;
        myGameManager.myGMInstance.setActivateObjects(false);
        goCharacterWoman.SetActive(myGameManager.myGMInstance.getCharacterWoman());
        goCharacterMan.SetActive(!myGameManager.myGMInstance.getCharacterWoman());

        HideNextScene();
        HideEscPanel();

        goNextButton.SetActive(false);

        myGameManager.myGMInstance.setActivateObjects(true);
    }

    void Update()
    {
        if (bLoadScene == true)
        {
            textLoading.enabled = true;
            textLoading.color = new Color(textLoading.color.r, textLoading.color.g, textLoading.color.b, Mathf.PingPong(Time.time, 1));
            //            labelLoading.tint = new Color(labelLoading.tint.r, labelLoading.tint.g, labelLoading.tint.b, Mathf.PingPong(Time.time, 1));
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ShowEscPanel();
        }

        if(bButtonNextActivated == false)
            if (myGameManager.myGMInstance.getPuzzleSolved(6) == true)
            {
                goNextButton.SetActive(true);
                bButtonNextActivated = true;
            }
    }
}
