﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class myScene02Manager : mySceneManager {

    public float fWaitSeconds;
    public GameObject goLabelScript;

    new public void Begining()
    {
        goLabelScript.SetActive(true);
        StartCoroutine(HideScript());
    }

    IEnumerator HideScript()
    {
        yield return new WaitForSeconds(fWaitSeconds);
        goLabelScript.SetActive(false);
        ActivateStars(true);
    }

    // Use this for initialization
    void Start () {
        textLoading.enabled = false;
        myGameManager.myGMInstance.setActivateObjects(false);
        goCharacterWoman.SetActive(myGameManager.myGMInstance.getCharacterWoman());
        goCharacterMan.SetActive(!myGameManager.myGMInstance.getCharacterWoman());

        goLabelScript.SetActive(false);

        ActivateStars(false);
        HideNextScene();
        HideEscPanel();

        ActivateObjects(fWaitSeconds);

        Begining();
    }
}
