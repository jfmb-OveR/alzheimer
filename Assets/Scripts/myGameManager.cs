﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class myGameManager : MonoBehaviour {

    public static myGameManager myGMInstance;
    public float fSeconsUntilAdsAppear;

    private static int[] aPuzzlesSolved = new int[7];

    private static bool bCharacterWoman = false;
    private bool bActivateObjects;

    #region getters
    public bool getActivateObjects()
    {
        return bActivateObjects;
    }

    public int getPuzzleSolve(int iId)
    {
        return (aPuzzlesSolved[iId]);
    }

    public bool getPuzzleSolved(int iId)
    {
        bool bSolved = false;

        if (aPuzzlesSolved[iId] == 1)
            bSolved = true;

//        Debug.Log("El puzzle " + iId + " está resuelto: " + bSolved);

        return bSolved;
    }

    public int getOnePlayerPref(int iId)
    {
        return (PlayerPrefs.GetInt("puzzle0" + iId.ToString()));
    }

    public bool getCharacterWoman()
    {
        return bCharacterWoman;
    }
    #endregion

    #region setters
    public void setActivateObjects(bool bNewValue)
    {
//        Debug.Log("Los objetos están activados?" + bNewValue);
        bActivateObjects = bNewValue;
    }

    public void setOnePlayerPref(int iId, int bValue)
    {
//        Debug.Log("Player pref: " + iId + "tiene valor " + bValue);
        PlayerPrefs.SetInt("puzzle0" + iId.ToString(), bValue);
    }

    public void setPuzzleSolve(int iId, int bValue)
    {
        aPuzzlesSolved[iId] = bValue;
        setOnePlayerPref(iId, bValue);
    }

    public void setCharacterWoman(bool bValue)
    {
        bCharacterWoman = bValue;
    }
    #endregion

    #region PersistenciaDatos
    public void resetPlayerPrefs()
    {
        for(int i = 0; i < aPuzzlesSolved.Length; i++)
            setOnePlayerPref(i, 0);
    }

    public void resetPuzzles()
    {
        for (int i = 0; i < aPuzzlesSolved.Length; i++)
        {
            aPuzzlesSolved[i] = 0;
            setOnePlayerPref(i, 0);
        }
    }

    public void savePlayerPrefs()
    {
        for(int i = 0; i < aPuzzlesSolved.Length; i++)
        {
            setOnePlayerPref(i, aPuzzlesSolved[i]);
        }
    }

    public void loadOnePlayerPref(int iId)
    {
        aPuzzlesSolved[iId] = getOnePlayerPref(iId);
    }

    public void loadAllPlayerPrefs()
    {
        for(int i = 0; i < aPuzzlesSolved.Length; i++)
        {
            aPuzzlesSolved[i] = getOnePlayerPref(i);
        }
    }
    #endregion

    public void ExitGame()
    {
        //UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        Application.Quit();
    }

    IEnumerator WaitToExit()
    {
        yield return new WaitForSeconds(2);
        ExitGame();
    }

    IEnumerator CorShowBanner(){
        yield return new WaitForSeconds(fSeconsUntilAdsAppear);
        AdmobScript.myAdmobScript_Instance.ShowBanner();
    }

    void Awake()
    {
        myGMInstance = this;

        loadAllPlayerPrefs();
    }

    void Start(){
        StartCoroutine(CorShowBanner());
    }
}
