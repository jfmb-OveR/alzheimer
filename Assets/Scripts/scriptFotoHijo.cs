﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptFotoHijo : MonoBehaviour {

    public GameObject goFotoMujer;
    public GameObject goFotoHombre;

    void Start()
    {
        goFotoMujer.SetActive(myGameManager.myGMInstance.getCharacterWoman());
        goFotoHombre.SetActive(!myGameManager.myGMInstance.getCharacterWoman());
    }
}
